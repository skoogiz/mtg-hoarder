package in.hoarder.mtg.app;

import com.google.gson.Gson;
import in.hoarder.mtg.deckbox.DeckboxExportCollector;
import in.hoarder.mtg.deckbox.DeckboxItem;
import in.hoarder.mtg.domain.Config;
import in.hoarder.mtg.item.ItemCollector;
import in.hoarder.mtg.item.ItemMatchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BasicApp {

    private static Logger logger = LoggerFactory.getLogger(BasicApp.class);

    private static Optional<Config> loadJsonConfig() {

        final Gson gson = new Gson();
        final File configFile = new File("src/main/resources", "config.json");

        try (Reader reader = new FileReader(configFile)) {

            Config.Input config = gson.fromJson(reader, Config.Input.class);

            final File file = new File(config.getFilePath());
            if (!file.exists()) {
                throw new FileNotFoundException("File '" + config.getFilePath() + "' not found.");
            }

            Config.Builder builder = new Config.Builder(file)
                    .editionNames(config.getEditions())
                    .rarityValues(config.getRarities());

            config.getPlaysetRules().forEach(rule -> builder.addPlaysetRule(rule.getRarity(), rule.getFullPlayset()));

            return Optional.of(builder.build());

        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }

        return Optional.empty();
    }

    public static void main(String... args) {
        Optional<Config> config = loadJsonConfig();
        config.ifPresent(conf -> {
            logger.info("Editions:");
            conf.getEditions().forEach(ed -> logger.info("> {}", ed.getName()));
            logger.info("Rarities:");
            conf.getRarities().forEach(r -> logger.info("> {}", r.name()));
            logger.info("Qty Rules:");
            conf.getPlaysetRules().forEach(r -> logger.info("> [{}] playset={}", r.appliesFor(), r.getFullPlayset()));

            ItemCollector<DeckboxItem> collector = new DeckboxExportCollector();
            List<DeckboxItem> items = collector.collect(conf.getFile())
                    .stream()
                    .filter(ItemMatchers.inEditions(conf.getEditions()))
                    .filter(DeckboxItem::isNonFoil)
                    .filter(ItemMatchers.inRarities(conf.getRarities()))
                    .filter(ItemMatchers.meetsPlaysetRule(conf.getPlaysetRules()))
                    .collect(Collectors.toList());
            if (items.isEmpty()) {
                logger.info("Set complete!");
            } else {
                items.forEach(item -> logger.info("{} {} ({})", item.getCount(), item.getName(), item.getEdition()));
            }
        });
        System.exit(0);
    }
}
