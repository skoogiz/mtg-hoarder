package in.hoarder.mtg.item;

import in.hoarder.mtg.domain.Edition;
import in.hoarder.mtg.domain.PlaysetRule;
import in.hoarder.mtg.domain.Rarity;

import java.util.List;
import java.util.function.Predicate;

public final class ItemMatchers {

    public static Predicate<Item> inEditions(List<Edition> editions) {
        return item -> (editions == null || editions.isEmpty()) || editions.stream().anyMatch(ed -> ed.matches(item.getEdition()));
    }

    public static Predicate<Item> inRarities(List<Rarity> rarities) {
        return item -> (rarities == null || rarities.isEmpty()) || rarities.stream().anyMatch(rarity -> rarity == Rarity.of(item.getRarity()));
    }

    public static Predicate<Item> meetsPlaysetRule(List<PlaysetRule> playsetRules) {
        return item -> {
            for (PlaysetRule rule : playsetRules) {
                if (rule.appliesFor() == Rarity.of(item.getRarity())) {
                    return item.getQuantity() < rule.getFullPlayset();
                }
            }
            return item.getQuantity() < 4;
        };
    }

    private ItemMatchers() {
    }
}
