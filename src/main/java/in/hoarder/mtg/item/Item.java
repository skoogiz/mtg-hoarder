package in.hoarder.mtg.item;

/**
 * @author skoogiz
 */
public interface Item {
    String getEdition();

    String getRarity();

    int getQuantity();
}
