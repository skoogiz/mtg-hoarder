package in.hoarder.mtg.domain;

import com.google.common.collect.ImmutableList;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

public interface Config {

    File getFile();

    List<Edition> getEditions();

    List<Rarity> getRarities();

    List<PlaysetRule> getPlaysetRules();

    class Builder {
        private final File file;
        private List<Edition> editions;
        private List<Rarity> rarities;
        private Map<Rarity, Integer> playsetRules = new HashMap<>();

        public Builder(File file) {
            this.file = file;
        }

        public Builder editions(List<Edition> editions) {
            this.editions = ImmutableList.copyOf(editions);
            return this;
        }

        public Builder editionNames(List<String> editions) {
            return editions(editions.stream().map(Edition::of).collect(Collectors.toList()));
        }

        public Builder rarities(List<Rarity> rarities) {
            this.rarities = ImmutableList.copyOf(rarities);
            return this;
        }

        public Builder rarityValues(List<String> rarities) {
            return rarities(rarities.stream().map(Rarity::of).collect(Collectors.toList()));
        }

        public Builder addPlaysetRule(Rarity rarity, int fullPlayset) {
            playsetRules.put(rarity, fullPlayset);
            return this;
        }

        public Builder addPlaysetRule(String rarity, int fullPlayset) {
            return addPlaysetRule(Rarity.of(rarity), fullPlayset);
        }

        public Builder addPlaysetRule(PlaysetRule playsetRule) {
            return addPlaysetRule(playsetRule.appliesFor(), playsetRule.getFullPlayset());
        }

        public Config build() {

            final List<PlaysetRule> playsetRuleList = new ArrayList<>();
            for (Rarity rarity : Rarity.values()) {
                if (playsetRules.containsKey(rarity)) {
                    playsetRuleList.add(PlaysetRule.create(rarity, playsetRules.get(rarity)));
                } else {
                    playsetRuleList.add(PlaysetRule.create(rarity));
                }
            }

            return new Config() {
                @Override
                public File getFile() {
                    return file;
                }

                @Override
                public List<Edition> getEditions() {
                    return editions != null ? editions : Collections.emptyList();
                }

                @Override
                public List<Rarity> getRarities() {
                    return rarities != null ? rarities : Collections.emptyList();
                }

                @Override
                public List<PlaysetRule> getPlaysetRules() {
                    return playsetRuleList;
                }
            };
        }
    }

    class Input {
        private String filePath;
        private List<String> editions;
        private List<String> rarities;
        private List<PlaysetRule.Input> playsetRules;

        public String getFilePath() {
            return filePath;
        }

        public List<String> getEditions() {
            return editions;
        }

        public List<String> getRarities() {
            return rarities;
        }

        public List<PlaysetRule.Input> getPlaysetRules() {
            return playsetRules;
        }
    }
}
