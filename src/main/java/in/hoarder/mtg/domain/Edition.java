package in.hoarder.mtg.domain;

import com.google.common.base.CharMatcher;
import com.google.common.base.Strings;

import java.util.function.Function;

public interface Edition {

    Function<String, String> TO_RAW_NAME = name -> CharMatcher.javaLetterOrDigit().retainFrom(name).toLowerCase();

    String getName();

    default boolean matches(String edition) {
        if (Strings.isNullOrEmpty(getName()) || Strings.isNullOrEmpty(edition)) return false;
        return TO_RAW_NAME.apply(getName()).equals(TO_RAW_NAME.apply(edition));
    }

    static Edition of(String edition) {
        return () -> edition;
    }
}
