package in.hoarder.mtg.domain;

public interface PlaysetRule {

    Rarity appliesFor();

    int getFullPlayset();

    static PlaysetRule create(Rarity rarity, int fullPlayset) {
        return new PlaysetRule() {
            @Override
            public Rarity appliesFor() {
                return rarity;
            }

            @Override
            public int getFullPlayset() {
                return fullPlayset;
            }
        };
    }

    static PlaysetRule create(Rarity rarity) {
        return create(rarity, 4);
    }

    class Input {

        private String rarity;

        private int fullPlayset;

        public String getRarity() {
            return rarity;
        }

        public int getFullPlayset() {
            return fullPlayset;
        }
    }
}
